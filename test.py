import unittest
import logging
from selenium import webdriver

class TestThree(unittest.TestCase):


    def test_click_on_article_tag(self):
        driver = webdriver.Chrome()
        driver.get('https://zen.yandex.ru/media/snowdance/kak-vziat-koshku-v-poezdku-chtoby-ei-bylo-komfortno-kak-podgotovit-koshku-k-poezdke-v-mashinepoezdesamolete-622264dc248c7e5a3d30e59a?&')
        tag_cats= driver.find_element_by_xpath('//*[@id="article__page-root"]/main/div[1]/div[1]/div[2]/article/div/div/p[32]/span/a[1]/span[2]/span')
        tag_cats.click()
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        self.assertEqual(driver.current_url, 'https://zen.yandex.ru/t/%D0%BA%D0%BE%D1%88%D0%BA%D0%B8')
        driver.close()

    def test_search(self):
        driver = webdriver.Chrome()
        driver.get('https://zen.yandex.ru/')
        searchbox = driver.find_element_by_xpath('/html/body/div[2]/div[1]/div[1]/div/div/div/div[2]/div/div/input[1]')
        searchbox.send_keys('some channel')
        mybutton = driver.find_element_by_xpath('/html/body/div[2]/div[1]/div[1]/div/div/div/div[2]/div/button')
        mybutton.click()
        self.assertEqual(driver.current_url, 'https://zen.yandex.ru/search?query=some%20channel')
        driver.close()

    def test_open_video(self):
        driver = webdriver.Chrome()
        driver.get('https://zen.yandex.ru/plugar_inf?lang=en&referrer_clid=1200')
        vid_link= driver.find_element_by_xpath('//*[@id="-16888384149117871"]/article/div/div/div[2]/div[1]/div[1]/div/div[2]/div/a')
        vid_link.click()
        self.assertEqual(len(driver.window_handles),2)
        driver.close()

if __name__ == '__main__':
    unittest.main()